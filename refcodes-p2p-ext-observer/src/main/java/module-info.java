module org.refcodes.p2p.ext.observer {
	requires transitive org.refcodes.p2p;
	requires transitive org.refcodes.observer;

	exports org.refcodes.p2p.ext.observer;
}
